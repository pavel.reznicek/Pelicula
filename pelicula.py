#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Pelicula
Producit pelicuam ex imaginibus partium longitudinis XXIV capturas.
In lingua anglica:
Produces a movie clip from images of movie parts 24 frames long.
"""

import wx
import wx.lib.filebrowsebutton
from cigydd.graphics import graphics
import pickle as pickle
import os

# Constantes:
MAGNITUDO_EDITORIS_NUMERICI = (50, -1)
STYLUS_PANELLI = wx.RAISED_BORDER
RATIO_RECTANGULI_IMAGINIS = 1.2
NUMERUS_IMAGINUM_PER_PARTEM = 24

class ApplicatioPeliculae(wx.App):
    "Applicatio peliculae"
    def __init__(self):
        wx.App.__init__(self, redirect=False)
        fenestrum = Fenestrum()
        self.SetTopWindow(fenestrum)
        fenestrum.Show()

class Fenestrum(wx.Frame):
    "Fenestrum capitale"
    def __init__(self, parens=None):
        wx.Frame.__init__(self, parent=parens, title="Pelicula",
                          style=wx.DEFAULT_FRAME_STYLE,
                          size=(640, 480))

        relOmnia = self.relOmnia = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(relOmnia)

        ############################################################################

        relPars = self.relPars = wx.BoxSizer(wx.VERTICAL)
        relOmnia.Add(relPars, 0, wx.GROW)

        panIndexPartis = self.panIndexPartis = wx.Panel(self, style=STYLUS_PANELLI)
        relPars.Add(panIndexPartis, 0, wx.GROW)

        sbIndexPartis = self.sbIndexPartis = wx.StaticBox(panIndexPartis,
                                                          label="Pars peliculae")
        relIndexPartis = self.relIndexPartis = wx.StaticBoxSizer(sbIndexPartis,
                                                                 wx.VERTICAL)
        panIndexPartis.Sizer = relIndexPartis

        edIndexPartis = self.edIndexPartis = wx.SpinCtrl(panIndexPartis, min=1,
                                                         initial=1,
                                                         size=MAGNITUDO_EDITORIS_NUMERICI)
        relIndexPartis.Add(edIndexPartis, 1, wx.GROW)

        fenImagoPartis = self.fenImagoPartis = \
            FenestrumCumPartePeliculae(self)
        relPars.Add(fenImagoPartis, 1, wx.GROW)


        ############################################################################

        relPunctaCrucialia = self.relPunctaCrucialia = wx.BoxSizer(wx.VERTICAL)
        relOmnia.Add(relPunctaCrucialia, 1, wx.GROW)

        relocatores = self.relocatores = []
        for r in range(3):
            index = int((NUMERUS_IMAGINUM_PER_PARTEM - 1) / 2.0 * r)
            reloc = Relocator(self, index_imaginis=index)
            relPunctaCrucialia.Add(reloc, 1, wx.GROW)
            relocatores.append(reloc)
        self.relocOrigo = relocatores[0]
        self.relocMedius = relocatores[1]
        self.relocFinis = relocatores[2]

        panEditio = self.panEditio = wx.Panel(self, style=STYLUS_PANELLI)
        relPunctaCrucialia.Add(panEditio, 0, wx.GROW)

        sbEditio = self.sbEditio = wx.StaticBox(panEditio,
                                                label="Editatio positionum capturarum")
        relEditio = self.relEditio = wx.StaticBoxSizer(sbEditio, wx.VERTICAL)
        panEditio.Sizer = relEditio

        # al = agellus logicus
        alParatus = self.alParatus = wx.CheckBox(panEditio,
                                                 label="– pro hanc peliculae partem "
                                                 "&parata est.")
        relEditio.Add(alParatus, 0, wx.GROW)

        ############################################################################

        panControlla = self.panControlla = wx.Panel(self, style=STYLUS_PANELLI)
        relOmnia.Add(panControlla, 0, wx.GROW)

        relControlla = self.relControlla = wx.BoxSizer(wx.VERTICAL)
        panControlla.SetSizer(relControlla)

        sbProiectum = self.sbProiectum = wx.StaticBox(panControlla,
                                                      label="&Proiectum")
        relProiectum = self.relProiectum = wx.StaticBoxSizer(sbProiectum,
                                                             wx.HORIZONTAL)
        relControlla.Add(relProiectum, 0, wx.GROW)

        prLegeProiectum = self.prLegeProiectum = wx.Button(panControlla,
                                                           label="&Lege proiectum")
        relProiectum.Add(prLegeProiectum, 1, wx.GROW)

        prSalvaProiectum = self.prSalvaProiectum = wx.Button(panControlla,
                                                             label="&Salva proiectum")
        relProiectum.Add(prSalvaProiectum, 1, wx.GROW)

        sbDirectorium = self.sbDirectorium = wx.StaticBox(panControlla,
                                                          label="&Imagines fonticae")
        relDirectorium = self.relDirectorium = wx.StaticBoxSizer(sbDirectorium,
                                                                 wx.VERTICAL)
        relControlla.Add(relDirectorium, 0, wx.GROW)

        edDirectorium = self.edDirectorium = \
            wx.lib.filebrowsebutton.DirBrowseButton(panControlla,
                                                    labelText="Directoruim cum imaginibus:",
                                                    dialogTitle="Selige directorium "
                                                    "cum imaginibus fonticis:",
                                                    buttonText="&Procházet")
        relDirectorium.Add(edDirectorium, 0, wx.GROW)

        prDirectorium = self.prDirectorium = \
            wx.Button(panControlla, label="&Lege imagines")
        relDirectorium.Add(prDirectorium, 0, wx.GROW)

        sbMagnitudoImaginis = self.sbMagnitudoImaginis = \
            wx.StaticBox(panControlla,
                         label="&Dimensiones capturae")
        relMagnitudoImaginis = self.relMagnitudoImaginis = wx.StaticBoxSizer(sbMagnitudoImaginis,
                                                                             wx.HORIZONTAL)
        relControlla.Add(relMagnitudoImaginis, 0, wx.GROW)

        stLatitudo = self.stLatitudo = wx.StaticText(panControlla, label="Latitudo:")
        relMagnitudoImaginis.Add(stLatitudo, 0, wx.CENTER | wx.RIGHT, 8)

        edLatitudo = self.edLatitudo = wx.SpinCtrl(panControlla, min=100,
                                                   max=1000, initial=100,
                                                   size=MAGNITUDO_EDITORIS_NUMERICI)
        relMagnitudoImaginis.Add(edLatitudo, 1, wx.CENTER | wx.RIGHT, 8)

        stAltitudo = self.stAltitudo = wx.StaticText(panControlla, label="Altitudo:")
        relMagnitudoImaginis.Add(stAltitudo, 0, wx.CENTER | wx.RIGHT, 8)

        edAltitudo = self.edAltitudo = wx.SpinCtrl(panControlla, min=100,
                                                   max=1000, initial=100,
                                                   size=MAGNITUDO_EDITORIS_NUMERICI)
        relMagnitudoImaginis.Add(edAltitudo, 1, wx.CENTER)

        sbProductio = self.sbProductio = wx.StaticBox(panControlla,
                                                      label="Productio")
        relProductio = self.relProductio = wx.StaticBoxSizer(sbProductio,
                                                             wx.VERTICAL)
        relControlla.Add(relProductio, 0, wx.GROW)

        tbProduca = self.ttProduca = wx.ToggleButton(panControlla,
                                                     label="&Produca peliculam")
        relProductio.Add(tbProduca, 0, wx.GROW)

        gaPars = self.gaPars = wx.Gauge(panControlla)
        relProductio.Add(gaPars, 0, wx.GROW)

        gaTotum = self.gaTotum = wx.Gauge(panControlla)
        relProductio.Add(gaTotum, 0, wx.GROW)

        ############################################################################

        prLegeProiectum.Bind(wx.EVT_BUTTON, self.prLegeProiectumPressus)
        prSalvaProiectum.Bind(wx.EVT_BUTTON, self.prSalvaProiectumPressus)

        prDirectorium.Bind(wx.EVT_BUTTON, self.prDirectoriumPressus)

        edIndexPartis.Bind(wx.EVT_SPINCTRL, self.edIndexPartisMutatus)

        edLatitudo.Bind(wx.EVT_SPINCTRL, self.edLatitudoVelAltitudoMutatus)
        edAltitudo.Bind(wx.EVT_SPINCTRL, self.edLatitudoVelAltitudoMutatus)

        for r in relocatores:
            r.Bind(EVT_RELOCATOR, self.RelocatorMutatus)

        alParatus.Bind(wx.EVT_CHECKBOX, self.alParatusMutatus)

        tbProduca.Bind(wx.EVT_TOGGLEBUTTON, self.ttProducaPressus)

        self.Bind(wx.EVT_CLOSE, self.CumConclusione, self)

        self.temProductio = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.temProductioActivatus)

        self.procProductio = wx.Process(self)
        self.Bind(wx.EVT_END_PROCESS, self.procProductioTerminatus)

        ############################################################################

        # inducabimus variabiles
        self.foliumImaginisPartis = ""
        self.bmpImagoPartis = None
        self.dirImaginum = None

        self.proiectum = Proiectum()

        self.LegeProiectum(nuntius=False)

        self.Fit()
        self.SetMinSize(self.Size)

    def CumConclusione(self, evt):
        "Laborat cum conclusione programmatis."
        resp = wx.MessageBox("Vuls proiectum salvare?", "Conclusio",
                             style=wx.YES|wx.NO|wx.CANCEL, parent=self)
        if resp == wx.CANCEL:
            if evt.CanVeto():
                evt.Veto()
            else:
                evt.Skip()
        elif resp == wx.YES:
            self.SalvaProiectum()
            evt.Skip()
        elif resp == wx.NO:
            evt.Skip()

    def prDirectoriumPressus(self, evt):
        "Laborat si pressor Directorium pressus est."
        self.proiectum.directorium = self.edDirectorium.GetValue()
        self.LegeDirectorium()
        evt.Skip()

    def prLegeProiectumPressus(self, evt):
        "Laborat si pressor Lege proiectum pressus est."
        self.LegeProiectum()
        evt.Skip()

    def LegeProiectum(self, nuntius=True):
        "Legit proiectum ex folio."
        proiectum = self.proiectum
        via = "proiectum.fpr"
        if os.path.exists(via):
            try:
                folium = open(via, "rb+")
                proiectum = self.proiectum = pickle.load(folium)
                error = False
            except IOError as error:
                wx.MessageBox("Error cum lectione proiecti: %s"%(error), "Proiectum",
                              parent=self)
                error = True

            # samotné načtení (popřípadě výchozích hodnot)
            self.edLatitudo.Value, self.edAltitudo.Value = proiectum.magnitudo_imaginis

            self.edDirectorium.SetValue(proiectum.directorium) # SetValue notum est
            self.LegeDirectorium()

            if nuntius and not error:
                wx.MessageBox("Lectio proiecti succidit :-)", "Proiectum",
                              parent=self)
        else:
            if nuntius:
                wx.MessageBox("Proiectum etiam non salvatum est ;-)", "Proiectum", parent=self)

    def LegeDirectorium(self):
        "Legit directorium cum imaginibus."
        proiectum = self.proiectum
        directorium = proiectum.directorium
        if os.path.exists(directorium) \
        and os.path.isdir(directorium) \
        and not proiectum.tabula_partium: # Modo si listam partium non habeamus
            folia = [ParsPeliculae(f) for f in os.listdir(directorium)
                     if f.lower().endswith(".png")]
            proiectum.tabula_partium = folia
            self.edIndexPartis.SetRange(1, len(proiectum.tabula_partium))
                # Ut limitum 100 non habeamus :-D
            self.LegeImaginemPartis()
            self.LegeImagines()

    def prSalvaProiectumPressus(self, evt):
        "Laborat si pressor Salva folium pressus est."
        self.SalvaProiectum()
        evt.Skip()

    def SalvaProiectum(self):
        "Salvat proiectum."
        proiectum = self.proiectum
        try:
            folium = open("proiectum.fpr", "wb+")
            pickle.dump(proiectum, folium, -1)
            wx.MessageBox("Proiectum cum successo salvatum est :-)", "Proiectum", parent=self)
        except IOError as error:
            wx.MessageBox("Error eventus est cum salvatione proiecti: %s"%error, "Proiectum",
                          parent=self)

    def edIndexPartisMutatus(self, evt):
        "Laborat si index partis peliculae mutatus est."
        self.LegeImaginemPartis()
        self.LegeImagines()
        evt.Skip()

    def LegeImaginemPartis(self):
        "Legit peliculae partis imaginem."
        ind = self.edIndexPartis.Value - 1
        if len(self.proiectum.tabula_partium) > 0:
            pars = self.proiectum.tabula_partium[ind]
            folium = pars.folium
        else:
            pars = None
            folium = ""
        if folium:
            if folium != self.foliumImaginisPartis:
                self.bmpImagoPartis = wx.Bitmap(os.path.join(self.proiectum.directorium,
                                                             folium))
        self.foliumImaginisPartis = folium
        self.fenImagoPartis.IndexPartis = ind
        self.fenImagoPartis.Proiectum = self.proiectum
        self.fenImagoPartis.CartaBitica = self.bmpImagoPartis
        if pars:
            self.alParatus.Value = pars.parata

    def edLatitudoVelAltitudoMutatus(self, evt):
        "Laborat si editor Latitudo vel editor Altitudo mutatus est."
        self.proiectum.magnitudo_imaginis = \
            (self.edLatitudo.Value, self.edAltitudo.Value)
        self.LegeImaginemPartis()
        self.LegeImagines()
        evt.Skip()

    def LegeImagines(self):
        "Legit imagines."
        for r in self.relocatores:
            reimagina = False
            if r.Proiectum != self.proiectum:
                r.Proiectum = self.proiectum
                reimagina = True
            if r.CartaBitica != self.bmpImagoPartis:
                r.CartaBitica = self.bmpImagoPartis
                reimagina = True
            if r.IndexPartis != self.edIndexPartis.Value - 1:
                r.IndexPartis = self.edIndexPartis.Value - 1
                reimagina = True
            if reimagina:
                r.Refresh()

    def RelocatorMutatus(self, evt):
        "Laborat cum relocatore mutato."
        self.LegeImaginemPartis()
        self.LegeImagines()
        evt.Skip()

    def alParatusMutatus(self, evt):
        "Laborat cum agello logico nomine Paratus mutato."
        pars = self.fenImagoPartis.ParsPeliculae
        if pars:
            pars.parata = self.alParatus.Value
            # Exhibimus nuntium, si pelicula secundum notas perfectionis partium
            # ad productionem parata est ;-)
            peliculaParata = True
            for p in self.proiectum.tabula_partium:
                if not p.parata:
                    peliculaParata = False
            if peliculaParata:
                wx.MessageBox("Secundum notas apud omnium partium peliculae haec "
                              "ad productionem parata est :-)", "Perfectum!", parent=self)
                self.ttProduca.Enabled = True
        evt.Skip()

    def ttProducaPressus(self, evt):
        "Laborat cum transtensore Produca deprimato."
        if self.ttProduca.Value:
            successus = self.ProducaImagines()
            if successus:
                self.ProducaPeliculam()
        else: # finire
            if self.temProductio.IsRunning():
                self.temProductio.Stop()
            if self.procProductio:
                wx.Process.Kill(self.procProductio.GetPid(), wx.SIGKILL)
        evt.Skip()

    def ProducaImagines(self):
        "Producat imagines."
        successus = False
        proiectum = self.proiectum
        dirImaginum = self.dirImaginum = \
            os.path.join(proiectum.directorium, "Capturae")
        if not os.path.exists(dirImaginum):
            os.mkdir(dirImaginum)
        self.gaTotum.Value = 0
        self.gaTotum.Range = len(proiectum.tabula_partium) - 1
        ni = 1 # numerus imaginis
        for p, pars in enumerate(proiectum.tabula_partium):
            numerusImaginumPartis = len(pars.imagines)
            self.gaPars.Range = numerusImaginumPartis - 1
            # carta bitica
            cbPartis = wx.Bitmap(os.path.join(proiectum.directorium,
                                              pars.folium))
            dcPartis = wx.MemoryDC(cbPartis)
            lp, ap = dcPartis.Size
            for i in range(len(pars.imagines)):
                li, ai = proiectum.magnitudo_imaginis
                cbImaginis = wx.EmptyBitmap(li, ai, cbPartis.Depth)
                dcImaginis = wx.MemoryDC(cbImaginis)
                # relocatio
                reloc = pars.relocatio_imaginis(i)
                # positio in imagine partis
                piipx, piipy = \
                    (lp - li) / 2 + reloc[0], \
                    (ap - ai * numerusImaginumPartis) / 2 + ai * i + reloc[1]
                dcImaginis.Blit(0, 0, li, ai, dcPartis, piipx, piipy)
                cbImaginis.SaveFile(os.path.join(dirImaginum,
                                                 str(ni).rjust(6, '0') + '.png'),
                                    wx.BITMAP_TYPE_PNG)
                ni += 1
                self.gaPars.Value = i
                wx.YieldIfNeeded()
                if not self.ttProduca.Value:
                    break
            self.gaTotum.Value = p
            wx.YieldIfNeeded()
            if not self.ttProduca.Value:
                break
        successus = self.ttProduca.Value
        return successus

    def ProducaPeliculam(self):
        "Producat peliculam resultantem."
        dirImaginum = self.dirImaginum
        foliaImaginum = os.path.join(dirImaginum, '%06d.png')
        foliumPeliculae = os.path.join(self.proiectum.directorium, 'pelicula.avi')
        if hasattr(self.proiectum, 'numerus_imaginum_per_partem'):
            nipp = self.proiectum.numerus_imaginum_per_partem
        else:
            nipp = NUMERUS_IMAGINUM_PER_PARTEM
        mandatum = 'ffmpeg -i "%s" -b 1024k -r %s "%s"'%(foliaImaginum, nipp, foliumPeliculae)
        self.procProductio = self.procProductio.Open(mandatum)
        self.procProductio.Bind(wx.EVT_END_PROCESS, self.procProductioTerminatus)
        self.procProductio.Redirect()
        if self.procProductio:
            self.temProductio.Start(1000)
        else:
            wx.MessageBox("Processum productionis peliculae iniciare non potuit.", "Error",
                          parent=self)

    def temProductioActivatus(self, evt):
        "Laborat cum temporatore Productio activato."
        if self.procProductio:
            fluxImputationis = self.procProductio.GetInputStream().read()
            print(fluxImputationis, end=' ')
            fluxErrorum = self.procProductio.GetErrorStream().read()
            print(fluxErrorum, end=' ')
        self.gaTotum.Pulse()
        evt.Skip()

    def procProductioTerminatus(self, evt):
        "Laborat cum processo Productio terminato."
        self.temProductio.Stop()
        self.ttProduca.Value = False
        ec = evt.ExitCode
        if ec == 0:
            nuntius = "Productio peliculae cum successo perfecta est :-)"
        elif ec != -1:
            nuntius = "Processus productionis finit cum errore numero %s."%ec
        else: # -1 - processus signali „SIGKILL“ terminatus est
            nuntius = ''
        s = self.procProductio.GetInputStream().read()
        print(s, end=' ')
        s = self.procProductio.GetErrorStream().read()
        print(s, end=' ')
        if nuntius:
            wx.MessageBox(nuntius, "Perfectum!",
                          parent=self)

EVTT_RELOCATOR = -1
class EventumRelocatoris(wx.NotifyEvent): # pylint: disable=too-few-public-methods
    "Classa servenda ut eventum relocatoris."
    def __init__(self):
        wx.NotifyEvent.__init__(self)
        self.EventType = EVTT_RELOCATOR
EVT_RELOCATOR = wx.PyEventBinder(EVTT_RELOCATOR)

class Relocator(wx.Panel):
    def __init__(self, parens, numerus_imaginum=NUMERUS_IMAGINUM_PER_PARTEM,
                 motio=(500, 500),
                 proiectum=None, index_partis=0, index_imaginis=0, carta_bitica=None,
                 stylus=STYLUS_PANELLI):
        wx.Panel.__init__(self, parent=parens, style=stylus)

        self.numerus_imaginum = numerus_imaginum
        self.motio = motio
        origo_vel_finis = \
            index_imaginis == 0 or index_imaginis == numerus_imaginum - 1

        relOmnia = self.relOmnia = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(relOmnia)

        fenImago = self.fenImago = FenestrumCumImaginePeliculae(self,
                                                                proiectum=proiectum,
                                                                index_partis=index_partis,
                                                                index_imaginis=index_imaginis,
                                                                carta_bitica=carta_bitica,
                                                                stylus=wx.DEFAULT)
        relOmnia.Add(fenImago, 1, wx.GROW | wx.RIGHT, 8)

        relControlla = self.Controlla = wx.BoxSizer(wx.VERTICAL)
        relOmnia.Add(relControlla, 0, wx.CENTER)

        relIndex = self.relIndex = wx.BoxSizer(wx.HORIZONTAL)
        relControlla.Add(relIndex, 1, wx.GROW)

        stIndex = self.stIndex = wx.StaticText(self, label="&Captura: ")
        relIndex.Add(stIndex, 1, wx.CENTER)

        # Cogitamus sibi extensionem vehiculae indexalis
        if origo_vel_finis:
            # Hic id non interest; dum iam variabilem hanc booleanam habemus,
            # nos sinimus extensionem 1 tenus imaginum numerum per partem peliculae
            mini, maxi = 1, numerus_imaginum
        else:
            # pro „medium“ relocatorem (non pertingit ad imagines extremas)
            mini, maxi = 2, numerus_imaginum - 1

        edIndex = self.edIndex = wx.SpinCtrl(self, min=mini, max=maxi,
                                             initial=index_imaginis + 1,
                                             size=MAGNITUDO_EDITORIS_NUMERICI)
        relIndex.Add(edIndex, 0, wx.CENTER)
        edIndex.Enabled = not origo_vel_finis

        # Si imago creat punctum suscipientem, captione indicatum erit
        titulus_puncti = self.titulus_puncti = "Punctum suscipiens"

        stPunctum = self.stPunctum = \
            wx.StaticText(self, style=wx.ALIGN_CENTER,
                          label=titulus_puncti * origo_vel_finis)
                          # ha ha, multiplicatio in Pythone :-D
        relControlla.Add(stPunctum, 0, wx.GROW)

        relX = self.relX = wx.BoxSizer(wx.HORIZONTAL)
        relControlla.Add(relX, 1, wx.GROW)

        stX = self.stX = wx.StaticText(self, label="&Costa sinistra: ")
        relX.Add(stX, 1, wx.CENTER)

        edX = self.edX = wx.SpinCtrl(self, min=-motio[0], max=motio[0],
                                     size=MAGNITUDO_EDITORIS_NUMERICI)
        relX.Add(edX, 0, wx.CENTER)

        relY = self.relY = wx.BoxSizer(wx.HORIZONTAL)
        relControlla.Add(relY, 1, wx.GROW)

        stY = self.stY = wx.StaticText(self, label="&Summitas: ")
        relY.Add(stY, 1, wx.CENTER)

        edY = self.edY = wx.SpinCtrl(self, min=-motio[1], max=motio[1],
                                     size=MAGNITUDO_EDITORIS_NUMERICI)
        relY.Add(edY, 0, wx.CENTER)

        ###########
        # Eventus #
        ###########

        edIndex.Bind(wx.EVT_SPINCTRL, self.edIndexMutatus)
        edX.Bind(wx.EVT_SPINCTRL, self.edXVelYMutatus)
        edY.Bind(wx.EVT_SPINCTRL, self.edXVelYMutatus)

    def DaX(self):
        imago = self.ImagoPeliculae
        if imago:
            return imago.x
        else:
            return 0
    def ObiciaX(self, val):
        self.edX.Value = val
        imago = self.ImagoPeliculae
        if imago and imago.x != val:
            imago.x = val
    X = property(DaX, ObiciaX)

    def DaY(self):
        imago = self.ImagoPeliculae
        if imago:
            return imago.y
        else:
            return 0
    def ObiciaY(self, val):
        self.edY.Value = val
        imago = self.ImagoPeliculae
        if imago and imago.y != val:
            imago.y = val
    Y = property(DaY, ObiciaY)

    def DaEdX(self):
        return self.edX
    EdX = property(DaEdX)

    def DaEdY(self):
        return self.edY
    EdY = property(DaEdY)

    def DaPartemPeliculae(self):
        if hasattr(self, 'proiectum') and self.proiectum:
            proiectum = self.proiectum
        if hasattr(self, 'index_partis'):
            return proiectum.tabula_partium[self.index_partis]
    ParsPeliculae = property(DaPartemPeliculae)

    def DaImaginemPeliculae(self):
        pars = self.ParsPeliculae
        if pars and hasattr(self, 'index_imaginis'):
            return pars.imagines[self.Index]

    def DaProiectum(self):
        if hasattr(self, 'proiectum') and self.proiectum:
            return self.proiectum
    def ObiciaProiectum(self, id_proiectum):
        self.fenImago.proiectum = id_proiectum
    Proiectum = property(DaProiectum, ObiciaProiectum)

    def DaIndicemPartis(self):
        return self.fenImago.index_partis
    def ObiciaIndicemPartis(self, index):
        self.fenImago.index_partis = index
    IndexPartis = property(DaIndicemPartis, ObiciaIndicemPartis)

    def DaIndicemImaginis(self):
        return self.fenImago.IndexImaginis
    def ObiciaIndicemImaginis(self, index):
        self.fenImago.IndexImaginis = index
        if self.edIndex.Value != index + 1:
            self.edIndex.Value = index + 1
    IndexImaginis = property(DaIndicemImaginis, ObiciaIndicemImaginis)

    def DaPartemPeliculae(self):
        return self.fenImago.ParsPeliculae

    def DaCartamBiticam(self):
        return self.fenImago.CartaBitica
    def ObiciaCartamBiticam(self, carta):
        self.fenImago.CartaBitica = carta
    CartaBitica = property(DaCartamBiticam, ObiciaCartamBiticam)

    def edIndexMutatus(self, evt):
        if self.IndexImaginis != self.edIndex.Value - 1:
            self.IndexImaginis = self.edIndex.Value - 1
            self.Refresh()

    def edXVelYMutatus(self, evt):
        reimagina = False
        if self.fenImago.ImagoPeliculae.x != self.edX.Value:
            self.fenImago.ImagoPeliculae.x = self.edX.Value
            reimagina = True
        if self.fenImago.ImagoPeliculae.y != self.edY.Value:
            self.fenImago.ImagoPeliculae.y = self.edY.Value
            reimagina = True
        if reimagina:
            self.stPunctum.Label = \
                self.titulus_puncti * self.fenImago.ImagoPeliculae.crucialis
        self.Layout() # remagnificationis tituli causa
        self.fenImago.Refresh()
        evtReloc = EventumRelocatoris()
        evtReloc.EventObject = self
        self.EventHandler.ProcessEvent(evtReloc)

    def Refresh(self):
        if self.edIndex.Value != self.IndexImaginis + 1:
            self.edIndex.Value = self.IndexImaginis + 1
        if self.edX.Value != self.fenImago.ImagoPeliculae.x:
            self.edX.Value = self.fenImago.ImagoPeliculae.x
        if self.edY.Value != self.fenImago.ImagoPeliculae.y:
            self.edY.Value = self.fenImago.ImagoPeliculae.y
        # Functio „Refresh“ originalis
        self.__class__.__base__.Refresh(self)

class Proiectum():
    def __init__(self):
        tabula_partium = self.tabula_partium = []
        directorium = self.directorium = ""
        magnitudo_imaginis = self.magnitudo_imaginis = (500, 300)
        self.numerus_imaginum_per_partem = NUMERUS_IMAGINUM_PER_PARTEM

class ParsPeliculae(object):
    def __init__(self, folium="", numerus_imaginum=NUMERUS_IMAGINUM_PER_PARTEM):
        object.__init__(self)
        self.folium = folium
        self.imagines = [ImagoPeliculae(i) for i in range(numerus_imaginum)]
        self._parata = False

    def DaImaginemOriginalem(self):
        if len(self.imagines) > 0:
            return self.imagines[0]
    imago_originalis = property(DaImaginemOriginalem)

    def DaImaginemFinalem(self):
        if len(self.imagines) > 0:
            return self.imagines[-1]
    imago_finalis = property(DaImaginemFinalem)

    def DaImaginesCruciales(self):
        return [i for i in self.imagines if i.crucialis]
    imagines_cruciales = property(DaImaginesCruciales)

    def DaUtParata(self):
        if hasattr(self, '_parata'):
            return self._parata
        else:
            return False
    def ScribeUtParata(self, ut_parata):
        self._parata = ut_parata
    parata = property(DaUtParata, ScribeUtParata)

    def imago_cum_indice(self, index):
        if index in range(len(self.imagines)):
            return self.imagines[index]

    def relocatio_imaginis(self, index):
        imago = self.imago_cum_indice(index)
        if imago.crucialis:
            return imago.x, imago.y
        else:
            # Quaerimur imaginem crucialem praecaedentem.
            # Initiamus ab indice minori.
            ind_praec = index - 1
            praec = self.imago_cum_indice(ind_praec)
            while not praec.crucialis:
                ind_praec -= 1
                praec = self.imago_cum_indice(ind_praec)
            # Quaerimur imaginem crucialem sequentem.
            # Initiamus ab indice maiori.
            ind_seq = index + 1
            seq = self.imago_cum_indice(ind_seq)
            while not seq.crucialis:
                ind_seq += 1
                seq = self.imago_cum_indice(ind_seq)
            # Habemus indices imaginum crucialium proximarum.
            # Nunc computamus differentiam coordinatarum et partem rationalem.
            differentia = seq.x - praec.x, seq.y - praec.y
            numerus_partium = ind_seq - ind_praec
            numerus_partium_ad_hoc_imaginem = index - ind_praec
            pars_rationalis = (d / float(numerus_partium) for d in differentia)
            # Computamus, quod addendum sit ad positionem imaginis crucialis
            # praecaedentis.
            # Facimus tupliam, quia obiectum generatoris causa quasi
            # subscriptabilis non est.
            addendum = tuple(p * numerus_partium_ad_hoc_imaginem
                             for p in pars_rationalis)
            # Nunc iam resultum computamus - addimus addendum ;-)
            resultum = praec.x + addendum[0], praec.y + addendum[1]
            return resultum
ParsFilmi = ParsPeliculae # pro compatibilitate

class ImagoPeliculae(object):
    def __init__(self, index=0, numerus_imaginum=NUMERUS_IMAGINUM_PER_PARTEM):
        self.x = 0
        self.y = 0
        self.index = index
        self._numerus_imaginum = numerus_imaginum

    def DaUtOriginalis(self):
        return self.index == 0
    originalis = property(DaUtOriginalis)

    def DaUtFinalis(self):
        return self.index == self._numerus_imaginum - 1
    finalis = property(DaUtFinalis)

    def DaUtCrucialis(self):
        return self.originalis or self.finalis or self.x != 0 or self.y != 0
    crucialis = property(DaUtCrucialis)
ImagoFilmi = ImagoPeliculae # pro compatibilitate

class FenestrumCumImagineRelata(wx.Panel):
    def __init__(self, parens=None, carta_bitica=None, proiectum=None,
                 index_partis=0, stylus=STYLUS_PANELLI):
        wx.Panel.__init__(self, parent=parens, style=stylus)
        self.Bind(wx.EVT_SIZE, self.CumRemagnificatione)
        self.carta_bitica = carta_bitica
        self.proiectum = proiectum
        self.index_partis = index_partis

    def CumRemagnificatione(self, evt):
        self.Refresh()

    def DaCartamBiticam(self):
        if hasattr(self, 'carta_bitica'):
            return self.carta_bitica
    def ScribeCartamBiticam(self, carta):
        self.carta_bitica = carta
        self.Refresh()
    CartaBitica = property(DaCartamBiticam, ScribeCartamBiticam)

    def DaRationem(self):
        cb = self.carta_bitica
        if cb:
            l, a = self.ClientSize
            lcb, acb = cb.Size
            return min(l / float(lcb), a / float(acb))
        else:
            return 1
    Ratio = property(DaRationem)

    def DaMagnitudinemInUnitatibusImaginis(self, magnitudo):
        return (i / self.Ratio for i in magnitudo)

    def DaMagnitudinemFenestraeInUnitatibusImaginis(self):
        return self.DaMagnitudinemInUnitatibusImaginis(self.ClientSize)
    MagnitudoFenestraeInUnitatibusImaginis = \
        property(DaMagnitudinemFenestraeInUnitatibusImaginis)

    def DaPunctumInUnitatibusImaginis(self, punctum):
        x, y = punctum
        l, a = self.DaMagnitudinemFenestraeInUnitatibusImaginis
        ratio = self.Ratio
        if self.carta_bitica:
            lcb, acb = self.carta_bitica.Size
        else:
            lcb, acb = l, a
        sinistrum, summitas = (i / 2 for i in (l - lcb, a - acb)) # sinistrum, summitas
        return (sinistrum + x / ratio, summitas + y / ratio)

    def DaMagnitudinemImaginisPartis(self):
        if self.carta_bitica:
            return self.carta_bitica.Size
        else:
            return self.ClientSize
    MagnitudoImaginisPartis = property(DaMagnitudinemImaginisPartis)

    def DaProiectum(self):
        return self.proiectum
    def ObiciaProiectum(self, proiectum):
        self.proiectum = proiectum
    Proiectum = property(DaProiectum, ObiciaProiectum)

    def DaIndicemPartis(self):
        return self.index_partis
    def ScribeIndicemPartis(self, index):
        self.index_partis = index
    IndexPartis = property(DaIndicemPartis, ScribeIndicemPartis)

    def DaPartemPeliculae(self):
        if hasattr(self, "proiectum") and self.proiectum:
            if self.index_partis in range(len(self.proiectum.tabula_partium)):
                return self.proiectum.tabula_partium[self.index_partis]
    ParsPeliculae = property(DaPartemPeliculae)

class FenestrumCumPartePeliculae(FenestrumCumImagineRelata):
    def __init__(self, parens, proiectum=None, index_partis=0,
                 carta_bitica=None):
        FenestrumCumImagineRelata.__init__(self, parens, carta_bitica)
        self.proiectum = proiectum
        self.index = index_partis
        self.Bind(wx.EVT_PAINT, self.CumImaginatione)
        #self.Bind(wx.EVT_SIZE, self.CumRemagnificatione)

    def CumImaginatione(self, evt):
        dc = wx.PaintDC(self)
        ratio = self.Ratio
        proiectum = self.proiectum
        pars = self.ParsPeliculae
        l, a = dc.Size
        if self.carta_bitica:
            mdc = wx.MemoryDC(self.carta_bitica)
            lcb, acb = mdc.Size # latitudo et altitudo cartae biticae
            dc.SetUserScale(ratio, ratio)
            ln, an = (i / ratio for i in dc.Size) # latitudo et altitudo nova
            dc.Blit((ln - lcb) / 2, (an - acb) / 2, lcb, acb, mdc, 0, 0)
            dc.SetUserScale(1, 1)
        if pars:
            dc.SetUserScale(ratio, ratio)
            if pars.imagines:
                for imago in pars.imagines:
                    if imago.crucialis:
                        dc.Pen = wx.Pen(wx.GREEN)
                    else:
                        dc.Pen = wx.Pen(wx.BLUE)
                    dc.Brush = wx.TRANSPARENT_BRUSH
                    l, a = self.MagnitudoFenestraeInUnitatibusImaginis
                    li, ai = self.MagnitudoImaginisPartis
                    lp, ap = self.proiectum.magnitudo_imaginis[0], \
                        self.proiectum.magnitudo_imaginis[1] * len(pars.imagines)
                    sinistrum, summitas = (i / 2 for i in (l - lp, a - ap))
                    #sinistrum, summitas = (i / 2 for i in (l - li, a - ai))
                    reloc = pars.relocatio_imaginis(imago.index)
                    dc.DrawRectangle(sinistrum + reloc[0],
                                     summitas + imago.index *
                                     proiectum.magnitudo_imaginis[1] +
                                     reloc[1],
                                     proiectum.magnitudo_imaginis[0],
                                     proiectum.magnitudo_imaginis[1])
                    dc.SetUserScale(1, 1)

class FenestrumCumImaginePeliculae(FenestrumCumImagineRelata):
    def __init__(self, parens, proiectum=None, index_partis=0, index_imaginis=0,
                 carta_bitica=None, stylus=STYLUS_PANELLI):
        FenestrumCumImagineRelata.__init__(self, parens=parens, proiectum=proiectum,
                                           index_partis=index_partis,
                                           carta_bitica=carta_bitica,
                                           stylus=stylus)
        self.index_imaginis = index_imaginis
        self.Bind(wx.EVT_PAINT, self.CumImaginatione)

    def CumImaginatione(self, evt):
        dc = wx.PaintDC(self)
        ratio = self.Ratio
        proiectum = self.proiectum
        pars = self.ParsPeliculae
        imago = self.ImagoPeliculae
        l, a = dc.Size
        if self.carta_bitica:
            mdc = wx.MemoryDC(self.carta_bitica)
            # latitudo et altitudo imaginis filmi
            li, ai = proiectum.magnitudo_imaginis
            # latitudo et altitudo imaginis partis
            lip, aip = mdc.Size
            # relocatio imaginis filmi computata de relocatione
            # imaginum crucialium proximorum
            reloc = pars.relocatio_imaginis(imago.index)
            positio_in_imagine_partis = (#(lip - li * RATIO_RECTANGULI_IMAGINIS) / 2
                (lip - l / ratio) / 2
                + reloc[0],
                # ad altitudinem: 15 × altitudo capturae
                # + quod pendit supra et infra ex excisurae
                (aip - (ai * len(pars.imagines) + (a / ratio - ai))) / 2
                + (ai * self.index_imaginis) + reloc[1])
            xpiip, ypiip = positio_in_imagine_partis
            dc.SetUserScale(ratio, ratio)
            dc.Blit(0, 0, l / ratio, a / ratio, mdc, xpiip, ypiip)
            dc.Brush = wx.TRANSPARENT_BRUSH
            dc.DrawRectangle(0, 0, l / ratio, a / ratio)
            if imago.crucialis:
                dc.Pen = wx.GREEN_PEN
            else:
                dc.Pen = wx.Pen(wx.BLUE)
            dc.DrawRectangle((l / ratio - li) / 2, (a / ratio - ai) / 2, li, ai)
            dc.SetUserScale(1, 1)


    def DaRationem(self):
        pr = self.proiectum
        if pr:
            l, a = self.ClientSize
            # Excisio peliculae partis imaginis, quod exhibimus:
            # aliqua pro cento pars ad marginem quemque obtingit ad striullam vicinitatis,
            # sicut nos possuamus bene speculare ;-)
            li, ai = (i * RATIO_RECTANGULI_IMAGINIS for i in pr.magnitudo_imaginis)
            return min(l / float(li), a / float(ai))
        else:
            return 1
    Ratio = property(DaRationem)

    def DaIndicemImaginis(self):
        return self.index_imaginis
    def ObiciaIndicemImaginis(self, index):
        self.index_imaginis = index
    IndexImaginis = property(DaIndicemImaginis, ObiciaIndicemImaginis)

    def DaImaginemPeliculae(self):
        pars = self.ParsPeliculae
        if pars:
            index_imaginis = self.IndexImaginis
            return pars.imago_cum_indice(index_imaginis)
    ImagoPeliculae = property(DaImaginemPeliculae)

if __name__ == '__main__':
    applicatio = ApplicatioPeliculae()
    applicatio.MainLoop()
