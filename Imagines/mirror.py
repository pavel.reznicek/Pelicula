#!/usr/bin/env python
# -*- coding: utf-8 -*-

import PIL.Image
import os

if __name__ == '__main__':

  for f in os.listdir(u'.'):
    if f.endswith(u'.png'):
      img = PIL.Image.open(f)
      img.file = f
      img.load()
      img = img.transpose(PIL.Image.FLIP_LEFT_RIGHT)
      img.save(f)
      print f
      
