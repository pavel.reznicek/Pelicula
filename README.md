#Pelicula
A Python utility which was used to reconstruct an animated clip from early
1980’s by Marta Vostrá, my sister, from a heavily damaged film footage
that she created in her school.

The resulting videos can be watched here:
https://borova18.dyndns.org/owncloud/s/8o8B5L9mFGe9iQ9

To see more recent Marta’s artwork, visit https://www.mravkolev.cz.

The film stripe was led through a scanner and scanned part by part,
we managed that each part had 24 frames (1 second).

I must admit that the code could look pretty strange for someone,
it uses Latin identifier names. At the time of coding it,
I didn’t count on publishing the source.
